namespace NormasApi.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}