using NormasApi.Data;
using NormasApi.Models;

namespace NormasApi.Service
{
    public class NormaService : BaseService<DataContext, Norma>
    {
        public NormaService(IDbContextFactory<DataContext> contextFactory)
            : base(contextFactory, c => c.Normas)
        {
        }
    }
}