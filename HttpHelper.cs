using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace NormasApi
{
    public static class HttpHelper
    {
        public static JsonSerializerOptions JsonSerializerOptions => new JsonSerializerOptions
        {
            IgnoreNullValues = false,
            WriteIndented = false,
            PropertyNameCaseInsensitive = true
        };
        
        public static string JsonSerialize<T>(T obj)
        {
            return JsonSerializer.Serialize(obj, JsonSerializerOptions);
        }
        
        public static string JsonSerialize<T>(T obj, JsonSerializerOptions options)
        {
            return JsonSerializer.Serialize(obj, options);
        }
        
        public static T JsonDeserialize<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json, JsonSerializerOptions);
        }
        
        public static T JsonDeserialize<T>(string json, JsonSerializerOptions options)
        {
            return JsonSerializer.Deserialize<T>(json, options);
        }
        
        public static HttpContent JsonContent<TContent>(TContent obj)
        {
            var jsonString = JsonSerialize(obj);
            const string mediaType = "application/json";
            var content = new StringContent(jsonString, Encoding.UTF8, mediaType);
            content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
            content.Headers.ContentLength = jsonString.Length;
            return content;
        }
        
        public static HttpContent JsonContent<TContent>(TContent obj, JsonSerializerOptions options)
        {
            var jsonString = JsonSerialize(obj, options);
            const string mediaType = "application/json";
            var content = new StringContent(jsonString, Encoding.UTF8, mediaType);
            content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
            content.Headers.ContentLength = jsonString.Length;
            return content;
        }
    }

}