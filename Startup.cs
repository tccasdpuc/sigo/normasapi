using System;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using NormasApi.Data;
using NormasApi.Middlewares;
using NormasApi.Models;
using NormasApi.PergamumGedWebInterface;
using NormasApi.Service;
using Polly;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Eureka;

namespace NormasApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<PergamumGedWebInterfaceSettings>(
                Configuration.GetSection(PergamumGedWebInterfaceSettings.SettingsKey));
            services.PostConfigure<PergamumGedWebInterfaceSettings>(settings =>
            {
                var envUrl = Environment.GetEnvironmentVariable("PERGAMUM_GEDWEB_INTERFACE_URL");
                if (Uri.TryCreate(envUrl, UriKind.Absolute, out var url)) settings.Url = url.ToString();
            });

            services
                .AddHttpClient<IPergamumGedWebInterfaceApi, PergamumGedWebInterfaceApi>(ConfigurePergamumApiHttpClient)
                .AddTransientHttpErrorPolicy(ConfigurePolicy);

            services.AddTransient<IDbContextFactory<DataContext>, DataContextFactory>();
            services.AddTransient<IRepository<Norma>, NormaService>();

            services.AddDiscoveryClient(Configuration);
            PostConfigureEureka(services);

            services.AddControllers();
            services.AddSwaggerGen();
        }

        private void ConfigurePergamumApiHttpClient(IServiceProvider serviceProvider, HttpClient httpClient)
        {
            var options = serviceProvider.GetService<IOptions<PergamumGedWebInterfaceSettings>>();
            var url = options.Value.Url;
            httpClient.BaseAddress = new Uri(url);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseDiscoveryClient();

            app.UseRouting();
            app.UseMiddleware<DeChunkerMiddleware>();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIGO Normas API"); });
        }

        private static IAsyncPolicy<HttpResponseMessage> ConfigurePolicy(PolicyBuilder<HttpResponseMessage> builder)
        {
            return builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(5)
            });
        }


        private void PostConfigureEureka(IServiceCollection services)
        {
            services.PostConfigure<EurekaClientOptions>(client =>
            {
                var envEurekaServiceUrl = Environment.GetEnvironmentVariable("EUREKA_SERVICE_URL");
                if (string.IsNullOrEmpty(envEurekaServiceUrl) == false
                    && Uri.TryCreate(envEurekaServiceUrl, UriKind.Absolute, out var url))
                    client.ServiceUrl = url.ToString();
            });

            services.PostConfigure<EurekaInstanceOptions>(instance =>
            {
                var envPort = Environment.GetEnvironmentVariable("PORT");
                if (string.IsNullOrEmpty(envPort) == false && ushort.TryParse(envPort, out var port))
                {
                    instance.Port = port;
                    instance.InstanceId = $"{instance.HostName}:{instance.AppName}:{instance.Port}".ToLower();
                }
            });
        }
    }
}