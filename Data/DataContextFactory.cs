using Microsoft.EntityFrameworkCore;

namespace NormasApi.Data
{
    public class DataContextFactory : IDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder.UseInMemoryDatabase("DataContext");
            return new DataContext(optionsBuilder.Options);
        }
    }
}