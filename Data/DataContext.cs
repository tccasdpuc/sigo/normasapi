using NormasApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory.ValueGeneration.Internal;

namespace NormasApi.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Norma> Normas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
              
        }

        
    }
}