using Microsoft.EntityFrameworkCore;

namespace NormasApi.Data
{
    public interface IDbContextFactory<out TContext> where TContext : DbContext
    {
        public TContext CreateDbContext();
    }
}