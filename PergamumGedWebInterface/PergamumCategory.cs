namespace NormasApi.PergamumGedWebInterface
{
    public class PergamumCategory
    {
        public string Nome { get; set; }
        public string UriSearch { get; set; }
        public int TotalRegistrosEncontrados { get; set; }
    }
}