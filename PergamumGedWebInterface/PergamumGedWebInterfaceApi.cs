using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace NormasApi.PergamumGedWebInterface
{
    public interface IPergamumGedWebInterfaceApi
    {
        Task<IList<Categoria>> GetCategoriasAsync(string busca);
        Task<IList<PergamumNorma>> GetNormasAsync(string busca, string codigoCategoria, int page = 1, int pageSize = 20);
        Task<LinkVisualizacao> GetLinkVisualizacaoAsync(string item);
    }

    public class PergamumGedWebInterfaceApi : IPergamumGedWebInterfaceApi
    {
        
        private readonly HttpClient _httpClient;
        private readonly ILogger<PergamumGedWebInterfaceApi> _logger;

        public PergamumGedWebInterfaceApi(HttpClient httpClient, ILogger<PergamumGedWebInterfaceApi> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }
        
        public Task<IList<Categoria>> GetCategoriasAsync(string busca)
        {
            var url = $"categorias?busca={busca}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return SendAsync<IList<Categoria>>(request);
        }

        public Task<IList<PergamumNorma>> GetNormasAsync(string busca, string codigoCategoria, int page = 1, int pageSize = 20)
        {
            var url = $"normas?busca={busca}&codigoCategoria={codigoCategoria}&page={page}&pageSize={pageSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return SendAsync<IList<PergamumNorma>>(request);
        }

        public Task<LinkVisualizacao> GetLinkVisualizacaoAsync(string item)
        {
            var url = $"linkVisualizacao?item=item";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            return SendAsync<LinkVisualizacao>(request);
        }
        
        private async Task SendAsync(HttpRequestMessage requestMessage)
        {
            try
            {
                var response = await _httpClient.SendAsync(requestMessage);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                //ignored
            }
        }

        private async Task<T> SendAsync<T>(HttpRequestMessage requestMessage) where T : class
        {
            var responseContent = "";
            try
            {
                var response = await _httpClient.SendAsync(requestMessage);
                response.EnsureSuccessStatusCode();
                responseContent = await response.Content.ReadAsStringAsync();
                var data = HttpHelper.JsonDeserialize<T>(responseContent);
                return data;
            }
            catch (HttpRequestException)
            {
                return null;
            }
            catch (JsonException e)
            {
                _logger.LogError($"Error Decoding Json ({e.Message}): {responseContent}");
                return null;
            }
        }

    }
}