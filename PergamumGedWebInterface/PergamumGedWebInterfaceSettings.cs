namespace NormasApi.PergamumGedWebInterface
{
    public class PergamumGedWebInterfaceSettings
    {
        public const string SettingsKey = "PergamumGedWebInterface";
        public string Url { get; set; }
    }
}