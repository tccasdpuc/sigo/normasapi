namespace NormasApi.PergamumGedWebInterface
{
    public class Categoria
    {
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public int TotalRegistrosEncontrados { get; set; }

    }
}