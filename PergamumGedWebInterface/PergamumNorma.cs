namespace NormasApi.PergamumGedWebInterface
{
    public class PergamumNorma
    {
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public string Data { get; set; }
        public  string UrlThumbnail { get; set; }
    }
}