using NormasApi.Data;
using NormasApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace NormasApi.Controllers
{
    [ApiController]
    [Route("")]
    public class NormaController : BaseController<Norma>
    {
        public NormaController(IRepository<Norma> repository) : base(repository)
        {
        }
    }
}