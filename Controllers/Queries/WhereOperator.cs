using System;
using System.Linq;
using System.Collections.Generic;

namespace NormasApi.Controllers.Queries
{
    public class WhereOperator : IComparable<WhereOperator>
    {
        private readonly string _value;

        private WhereOperator(string value)
        {
            var values = new[] {"in", "=", "!=", ">", "<", ">=", "<=", "like"};
            if (values.Contains(value) == false) throw new Exception("Invalid Operator");
            _value = value;
        }

        public static WhereOperator In => new WhereOperator("in");
        public static WhereOperator Eq => new WhereOperator("=");
        public static WhereOperator Ne => new WhereOperator("!=");
        public static WhereOperator Gt => new WhereOperator(">");
        public static WhereOperator Lt => new WhereOperator("<");
        public static WhereOperator Ge => new WhereOperator(">=");
        public static WhereOperator Le => new WhereOperator("<=");
        public static WhereOperator Lk => new WhereOperator("like");

        public static IEqualityComparer<WhereOperator> ValueComparer { get; } = new ValueEqualityComparer();

        public static bool operator ==(WhereOperator left, WhereOperator right)
        {
            return left?.Equals(right) ?? false;
        }

        public static bool operator !=(WhereOperator left, WhereOperator right)
        {
            return left == right == false;
        }

        public static WhereOperator Parse(string value)
        {
            return new WhereOperator(value);
        }

        public int CompareTo(WhereOperator other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(_value, other._value, StringComparison.Ordinal);
        }

        protected bool Equals(WhereOperator other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((WhereOperator) obj);
        }

        public override int GetHashCode()
        {
            return _value != null ? _value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return _value;
        }

        private sealed class ValueEqualityComparer : IEqualityComparer<WhereOperator>
        {
            public bool Equals(WhereOperator x, WhereOperator y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x._value == y._value;
            }

            public int GetHashCode(WhereOperator obj)
            {
                return obj._value != null ? obj._value.GetHashCode() : 0;
            }
        }
    }
}