using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NormasApi.PergamumGedWebInterface;

namespace NormasApi.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("pergamum")]
    public class PergamumController : ControllerBase
    {
        private readonly IPergamumGedWebInterfaceApi _api;

        public PergamumController(IPergamumGedWebInterfaceApi api)
        {
            _api = api;
        }   
        
        [HttpGet("categorias")]
        public Task<IList<Categoria>> GetCategorias([FromQuery] [Required] string busca)
        {
            return  _api.GetCategoriasAsync(busca);
        }
        
        [HttpGet("normas")]
        public Task<IList<PergamumNorma>> GetNormas(
            [FromQuery] [Required] string busca,
            [FromQuery] [Required] string codigoCategoria,
            [FromQuery] int page = 1,
            [FromQuery] int pageSize = 20
        )
        {
            return _api.GetNormasAsync(busca, codigoCategoria, page, pageSize);
        }
        
        [HttpGet("linkVisualizacao")]
        public Task<LinkVisualizacao> GetLinkVisualizacao([FromQuery] [Required] string item)
        {
            return _api.GetLinkVisualizacaoAsync(item);
        }

    }
}